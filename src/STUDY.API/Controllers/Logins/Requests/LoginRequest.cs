﻿namespace STUDY.API.Controllers.Logins.Requests
{
    public class LoginRequest
    {
        public string Email { get; set; }
    }
}
