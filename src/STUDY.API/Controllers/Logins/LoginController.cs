﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using STUDY.API.Controllers.Logins.Requests;
using STUDY.API.ViewModel;
using STUDY.Application.Abstraction.Logins.Commands;
using STUDY.Application.Abstraction.Logins.Responses;
using System.Threading.Tasks;

namespace STUDY.API.Controllers.Logins
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IMediator _mediator;

        public LoginController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ComumResponseViewModel<LoginJWTResponse>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Login([FromBody] LoginRequest model)
        {
            var command = new LoginCommand(model.Email);
            var result = await this._mediator.Send(command);

            return Ok(result);
        }

        [HttpPost("ControllerDemonstrativo")]
        [AllowAnonymous]
        public async Task<IActionResult> ControllerDemonstrativo()
        {
            return Ok("Controller adicionado para demonstração de pipeline!");
        }
    }
}
