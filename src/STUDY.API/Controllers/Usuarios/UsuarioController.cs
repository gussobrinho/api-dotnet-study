﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using STUDY.API.Controllers.Usuarios.Requests;
using STUDY.API.ViewModel;
using STUDY.Application.Abstraction.Usuarios.Commands;
using STUDY.Application.Abstraction.Usuarios.Querys;
using STUDY.Application.Abstraction.Usuarios.Responses;
using System.Threading.Tasks;

namespace STUDY.API.Controllers.Usuarios
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsuarioController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost("Cadastrar")]
        [Authorize("Bearer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ComumResponseViewModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CadastrarUsuario([FromBody] AdicionarUsuarioRequest model)
        {
            var command = new AdicionarUsuarioCommand(model.Nome, model.Email);
            await this._mediator.Send(command);

            return Ok();
        }

        [HttpGet("BuscarPorEmail")]
        [Authorize("Bearer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ComumResponseViewModel<BuscarUsuarioPorEmailResponse>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> BuscarPorEmail([FromQuery] string email)
        {
            var query = new BuscarUsuarioPorEmailQuery(email);
            var response = await this._mediator.Send(query);

            return Ok(response);
        }
    }
}
