﻿namespace STUDY.API.ViewModel
{
    public class ComumResponseViewModel
    {
        public ComumResponseViewModel(bool sucesso, string messagem = null)
        {
            Sucesso = sucesso;
            Messagem = messagem;
        }

        public bool Sucesso { get; set; }

        public string Messagem { get; set; }
    }
}
