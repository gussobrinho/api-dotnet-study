﻿namespace STUDY.API.ViewModel
{
    public class ComumResponseViewModel<T> : ComumResponseViewModel
    {
        public ComumResponseViewModel(bool sucesso, T data, string messagem = null)
            : base(sucesso, messagem)
        {
            this.Data = data;
        }

        public T Data { get; set; }
    }
}
