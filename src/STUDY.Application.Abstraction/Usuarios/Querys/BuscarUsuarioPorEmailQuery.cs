﻿using MediatR;
using STUDY.Application.Abstraction.Usuarios.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace STUDY.Application.Abstraction.Usuarios.Querys
{
    public class BuscarUsuarioPorEmailQuery : IRequest<BuscarUsuarioPorEmailResponse>
    {
        public string Email { get; set; }

        public BuscarUsuarioPorEmailQuery(string email)
        {
            this.Email = email;
        }
    }
}
