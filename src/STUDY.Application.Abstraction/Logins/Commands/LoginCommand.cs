﻿using MediatR;
using STUDY.Application.Abstraction.Logins.Responses;

namespace STUDY.Application.Abstraction.Logins.Commands
{
    public class LoginCommand : IRequest<LoginJWTResponse>
    {
        public string Email { get; set; }

        public LoginCommand(string email)
        {
            this.Email = email;
        }
    }
}
