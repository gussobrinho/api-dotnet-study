﻿using STUDY.Domain.Common;
using System;

namespace STUDY.Domain.Usuarios
{
    public class Usuario : BaseEntity
    {
        public string Nome { get; protected set; }
        public string Email { get; protected set; }


        private Usuario()
        {

        }

        public static Usuario New(string nome, string email)
        {
            var usuario = new Usuario();

            usuario.Nome = nome;
            usuario.Email = email;
            usuario.Ticket = Guid.NewGuid();

            return usuario;
        }
    }
}
