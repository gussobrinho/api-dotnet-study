﻿using System.Threading.Tasks;

namespace STUDY.Domain.Usuarios
{
    public interface IUsuarioService
    {
        Task AdicionarUsuario(string nome, string email);
    }
}
