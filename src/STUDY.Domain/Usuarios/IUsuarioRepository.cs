﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace STUDY.Domain.Usuarios
{
    public interface IUsuarioRepository
    {
        Task Add(Usuario usuario);
        Task<List<Usuario>> FindAll();
        Task<Usuario> FindByEmail(string email);
    }
}
