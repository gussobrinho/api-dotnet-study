﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using STUDY.Application;
using STUDY.Application.Abstraction;
using STUDY.Domain;
using STUDY.Exceptions;
using STUDY.Infrastructure.Authentication;
using STUDY.Infrastructure.Repository;
using System.Net.Http;
using System.Reflection;

namespace STUDY.IoC
{
    public static class Bootstrapper
    {
        private static Assembly[] assemblies = new[]
        {
            Assembly.GetEntryAssembly(),
            typeof(Domain.Bootstrapper).Assembly,
            typeof(Application.Bootstrapper).Assembly,
            typeof(Application.Abstraction.Bootstrapper).Assembly,
            typeof(Infrastructure.Repository.Bootstrapper).Assembly,
            typeof(Exceptions.Bootstrapper).Assembly,
            typeof(Infrastructure.Authentication.Bootstrapper).Assembly,
        };

        public static IServiceCollection AddBootstrapperIoC(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddBootstrapperDomain();
            services.AddBootstrapperApplication();
            services.AddBootstrapperAbstraction();
            services.AddBootstrapperExceptions();
            services.AddBootstrappeAutorizacaoService();
            services.AddBootstrapperRepository(configuration);

            services.AddScoped<HttpClient>(s => new HttpClient(new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, certificate, arg3, arg4) => true
            }));

            services.AddValidations();

            services.AddAutoMapper(assemblies, ServiceLifetime.Transient);

            return services;
        }

        public static IServiceCollection AddValidations(this IServiceCollection services)
        {
            AssemblyScanner.FindValidatorsInAssemblies(assemblies).ForEach(f =>
            {
                services.AddTransient(f.InterfaceType, f.ValidatorType);
                services.AddTransient(f.ValidatorType, f.ValidatorType);
            });

            return services;
        }
    }
}
