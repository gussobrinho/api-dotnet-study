﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STUDY.Exceptions.Common
{
    public class FalhaNaAutenticacaoException : DomainException
    {
        public FalhaNaAutenticacaoException(string message) : base(message)
        {

        }
    }
}
