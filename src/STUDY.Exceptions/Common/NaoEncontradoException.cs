﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STUDY.Exceptions.Common
{
    public class NaoEncontradoException : DomainException
    {
        public NaoEncontradoException(string message)
        : base(message)
        {
        }
    }
}
