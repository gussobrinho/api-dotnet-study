﻿using Microsoft.EntityFrameworkCore;

namespace STUDY.Infrastructure.Repository.Context
{
    public class StudyDbContext : DbContext
    {
        public StudyDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(StudyDbContext).Assembly);
        }
    }
}
