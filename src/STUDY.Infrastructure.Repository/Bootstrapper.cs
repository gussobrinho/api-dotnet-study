﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using STUDY.Domain.Usuarios;
using STUDY.Infrastructure.Repository.Context;
using STUDY.Infrastructure.Repository.Repositories;

namespace STUDY.Infrastructure.Repository
{
    public static class Bootstrapper
    {
        public static IServiceCollection AddBootstrapperRepository(this IServiceCollection services, IConfiguration configuration, string configConnectionString = "ConnectionString")
        {
            services
               .AddDbContext<StudyDbContext>((s, options) =>
               {
                   options.UseNpgsql(configuration.GetSection(configConnectionString).Value);
               })
               .AddScoped<DbContext>(s => s.GetService<StudyDbContext>());

            services.AddScoped<IUsuarioRepository, UsuarioRepository>();

            return services;
        }
    }
}