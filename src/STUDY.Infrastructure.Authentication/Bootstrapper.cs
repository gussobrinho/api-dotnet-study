﻿using Microsoft.Extensions.DependencyInjection;
using STUDY.Infrastructure.Authentication.Security;

namespace STUDY.Infrastructure.Authentication
{
    public static class Bootstrapper
    {
        public static IServiceCollection AddBootstrappeAutorizacaoService(this IServiceCollection services)
        {
            services
                .AddScoped<AuthenticationService>();

            services
                .AddSingleton<SigningConfigurations>()
                .AddSingleton<TokenConfigurations>();

            return services;
        }
    }
}
