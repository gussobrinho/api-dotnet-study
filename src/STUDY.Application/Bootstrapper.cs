﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using STUDY.Application.Logins.Commands;
using STUDY.Application.Usuarios.Commands;
using STUDY.Application.Usuarios.Querys;

namespace STUDY.Application
{
    public static class Bootstrapper
    {
        public static IServiceCollection AddBootstrapperApplication(this IServiceCollection services)
        {
            #region Commands
            services
                .AddMediatR(typeof(AdicionarUsuarioCommandHandler).Assembly)
                .AddMediatR(typeof(LoginCommandHandler).Assembly);
            #endregion

            #region Querys
            services
                .AddMediatR(typeof(BuscarUsuarioPorEmailQueryHandler).Assembly);
            #endregion

            return services;
        }
    }
}
