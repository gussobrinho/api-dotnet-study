﻿using MediatR;
using STUDY.Application.Abstraction.Usuarios.Commands;
using STUDY.Domain.Usuarios;
using System.Threading;
using System.Threading.Tasks;

namespace STUDY.Application.Usuarios.Commands
{
    public class AdicionarUsuarioCommandHandler : IRequestHandler<AdicionarUsuarioCommand>
    {
        private readonly IUsuarioService _service;

        public AdicionarUsuarioCommandHandler(IUsuarioService service)
        {
            this._service = service;
        }

        public async Task<Unit> Handle(AdicionarUsuarioCommand request, CancellationToken cancellationToken)
        {
            await this._service.AdicionarUsuario(request.Nome, request.Email);

            return Unit.Value;
        }
    }
}
