using Xunit;

namespace API.Tests.GenericTests.UnitTest
{
    public class TesteGenericoSucessoFalha
    {
        [Fact]
        public void Teste_Generico_Sucesso()
        {
            var result = 1 == 1 ? true : false;

            Assert.True(result);
        }

        [Fact]
        public void Teste_Generico_Erro()
        {
            var result = 1 == 2 ? true : false;

            Assert.False(result);
        }
    }
}
