FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build

WORKDIR ./

#EXPOSE 8080
#EXPOSE 80
#EXPOSE 443

#Copias de arquivos para a imagem
COPY ./src ./src

WORKDIR "./src/STUDY.API"

#Build
RUN dotnet build -c Release -o /app/build

#Publish
FROM build AS publish
RUN dotnet publish -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:3.1
ENV ASPNETCORE_URLS=http://+:8080

WORKDIR /app
COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet", "STUDY.API.dll"]
CMD ASPNETCORE_URLS=http://*:$PORT dotnet STUDY.API.dll